[CLARIN dSpace](https://github.com/ufal/clarin-dspace) comes with the discojuice discovery service configured out of the box. The discojuice discovery service has 
been discontinued since .... This guide describes the steps required to update the dSpace configuration in order to use 
the CLARIN (or any other) 

Based on the work provided by [Riccardo del Gratta](http://www.ilc.cnr.it/en/content/riccardo-del-gratta).

# Update shibboleth

REMEMBER TO CONFIGURE shibboleth2.xml:
in the `<Application />` part use
```
<SSO
 discoveryProtocol="SAMLDS"
discoveryURL="https://discovery.clarin.eu/discojuice">
  SAML2 SAML1
</SSO>
```
Where `discoveryURL="https://discovery.clarin.eu/discojuice"` is pointing to the CLARIN discovery service. Feel free to
change this to point to any other discovery service you want to use.

# Customizing CLARIN dSpace

Overlays are a way of introducing your local changes to themes or even source code without making the working directory 
dirty. See https://github.com/ufal/clarin-dspace/wiki/Overlays for more information on the use of overlays in the CLARIN 
dSpace setup.

For this guide overlays will be used to overwrite the out-of-the-box discovery service configuration.

Overlays can be found under `[dspace-source]/dspace/modules/`.

## Configure authentication options

### Local authentication and federated login via SAML (shibboleth)

Edit the `authenticationMethod` property in `dspace.cfg` as follows:
```
authenticationMethod=org.dspace.authenticate.PasswordAuthentication, cz.cuni.mff.ufal.dspace.authenticate.ShibAuthentication
```

### Federated loging only

Edit the `authenticationMethod` property in `dspace.cfg` as follows:
```
authenticationMethod=cz.cuni.mff.ufal.dspace.authenticate.ShibAuthentication
```
In this case only one authentication method is configured.

## Configure federated login

### Federated Login with login chooser

Local authentication is strongly suggested if you want a kind of backdoor for local administrators.

Files:
messages.xml (`[dspace-source][dspace-source]/dspace/modules/xmlui/src/main/webapp/i18n/messages.xml`). 

Changes:
```
...
<message key="xmlui.EPerson.Navigation.login">Local Authentication</message>
...
<message key="xmlui.EPerson.Navigation.discojuice.login">Federated Login</message>
...
```
Navigation.xml (`[dspace-source]/dspace/modules/xmlui/src/main/java/org/dspace/app/xmlui/aspect/eperson/Navigation.java`).

Changes:

Comment out line 90:
```
private static final Message T_login = message("xmlui.EPerson.Navigation.login"); // T_Login is the pointer to the message key xmlui.EPerson.Navigation.login
```
add an item after line 227
```
account.addItem().addXref(contextPath + "/password-login", T_login, "local password"); // use a class <> signon. Signon triggers discojuice. I've to found how to add a new icon
```
LoginChooser.java (`[dspace-source]xmlui/src/main/java/org/dspace/app/xmlui/aspect/eperson/LoginChooser.java`). 
Follow:
https://github.com/ufal/clarin-dspace/pull/982/files#diff-4835248b0b9dcb9e56442fd22a50f471b7ac62e5b3acff4b43496699267aecfd


aai_config.js (`[dspace-source]xmlui/src/main/webapp/themes/UFAL/lib/js/aai_config.js`). Follow:
https://github.com/ufal/clarin-dspace/pull/982/files#diff-4835248b0b9dcb9e56442fd22a50f471b7ac62e5b3acff4b43496699267aecfd

Comments
Case A adds a local authentication link to the right panel of the GUI. By clicking on the link, you are sent to the password-login page. Just provide the credential as local admin.
Case A uses the login link at the right panel and on top to send you to a login chooser, as described in the issue https://github.com/ufal/clarin-dspace/pull/982/files#diff-4835248b0b9dcb9e56442fd22a50f471b7ac62e5b3acff4b43496699267aecfd. You can log using either Shibboleth or local authentication. 


### Federated Login, no Login Chooser

Follow the steps to configure federated login. Then edit `sitemap.xmap` (`xmlui/src/main/resources/aspects/EPerson/sitemap.xmap`). 

Changes: at line 153: 
```
<map:when test="1"> 
<map:act type="LoginRedirect" /> 
</map:when> To 
<map:when test="2">  <!-- maybe any number different from 1 -->
<map:act type="LoginRedirect" /> 
</map:when>
```

Note: 

Login methods are defined in `<path>/dspace/config/dspace.cfg`. THE IMPORTANT THING HERE IS THAT the property 
`authenticationMethod` MUST have `cz.cuni.mff.ufal.dspace.authenticate.ShibAuthentication` as its last item. 

For example:
```
authenticationMethod=org.dspace.authenticate.PasswordAuthentication, cz.cuni.mff.ufal.dspace.authenticate.ShibAuthentication.
```
This workaround sets the last method as the redirect url. In this way the login path is triggered and shibboleth is executed.

### Federated Login, no Login  (alternative)

We can also change `xmlui/src/main/java/org/dspace/app/xmlui/aspect/eperson/LoginRedirect.java` 

LoginRedirect (`xmlui/src/main/java/org/dspace/app/xmlui/aspect/eperson/LoginRedirect.java`). 

Changes:
add this code under line 54 AuthenticationMethod shibMethod = null; 
```
//COMMENT THE EXCEPTION AND INIT the shibMethod
if (authMethod != null)
              {
                    //throw new IllegalStateException(
                      //      "Multiple explicit authentication methods found when only one was expected.- "+currAuthMethod.toString() +" -");
                }
                authMethod = currAuthMethod;
                if (currAuthMethod.toString().contains("ShibAuthentication")){
                        shibMethod=currAuthMethod;
                }
```
then change from
```
 final String url = ((AuthenticationMethod) authMethod).loginPageURL(
                ContextUtil.obtainContext(objectModel), httpRequest,
                httpResponse);
```
to 
```
final String url = ((AuthenticationMethod) shibMethod).loginPageURL(
                ContextUtil.obtainContext(objectModel), httpRequest,
                httpResponse);
```
This saves you to need to have the shibMethod as the last one in dspace.cfg
