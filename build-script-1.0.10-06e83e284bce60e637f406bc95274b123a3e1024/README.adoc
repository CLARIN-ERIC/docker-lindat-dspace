= CLARIN-ERIC docker build workflow
:caution-caption: ☡ CAUTION
:important-caption: ❗ IMPORTANT
:note-caption: 🛈 NOTE
:sectanchors:
:sectlinks:
:sectnumlevels: 6
:sectnums:
:source-highlighter: pygments
:tip-caption: 💡 TIP
:toc-placement: preamble
:toc:
:warning-caption: ⚠ WARNING

This project contains all scripts and docker images which are required for the CLARIN-ERIC docker workflow.

== Dependencies

[options="header",cols=",,,m"]
|===
| Conditions | Type | Name (URL) | Version constraint

| by necessity
| software
| https://www.docker.com/[Docker Compose]
| ==1.8.0

| by necessity
| software
| https://www.docker.com/[Docker Engine]
| ==1.11.2

| by necessity
| image
| https://github.com/gliderlabs/docker-alpine[`gliderlabs/alpine`]
| ==3.4

| for releases
| platform
| https://about.gitlab.[GitLab CI]
| ==8.10.4

|===

== Goals

Provide a uniform build, test and release workflow, both locally and within the gitlab platform
which allows customization where needed.

== Installing

=== Automated
Install the build script in your git repo with the following, `curl pipe bash,` command:

[source,sh]
----
curl -s -L https://gitlab.com/CLARIN-ERIC/build-script/raw/master/init_repo.sh | bash
----

=== Manual initialization

In the following sub-sections you'll find a breakdown of the commands run by the script.

==== Download and extract release

[source,sh]
----
VERSION=1.0.10 &&
curl https://gitlab.com/CLARIN-ERIC/build-script/repository/archive.tar.gz?ref=${VERSION} | tar xz && \
ln -s build-script-${VERSION}-*/build.sh build.sh && \
ln -s build-script-${VERSION}-*/copy_data_noop.sh copy_data.sh
----

==== Prepare the directory structure

[source.sh]
----
mkdir image run
touch image/.gitkeep
touch run/.gitkeep
----

==== Intialize local git repo

[source,sh]
----
git init
git add .
git commit -m "Intialized empty repo with build script v${VERSION}"
----

=== Output

Running the automated `curl pipe bash` command or doing it manually should result in a directory structure similar to
the following:

[source, sh]
----
.
├── build-script-1.0.9-7d6e1347bc80a5542d939624be46475f84ce11c2
│   ├── LICENSE.txt
│   ├── README.adoc
│   ├── build.sh
│   └── copy_data_noop.sh
├── build.sh -> build-script-1.0.9-7d6e1347bc80a5542d939624be46475f84ce11c2/build.sh
├── copy_data.sh -> build-script-1.0.9-7d6e1347bc80a5542d939624be46475f84ce11c2/copy_data_noop.sh
├── image
└── run

3 directories, 6 files
----

== Upgrading

=== Automated

<to be done>

=== Manual
1. Remove any existing build.sh and copy_data.sh files.
2. Run the installation instructions

Remove existing files and directory:

[source,sh]
----
rm build.sh
rm copy_data.sh
rm -r build-script-${PREVIOUS_VERSION}-*
----


== Customizing

=== Test Runner

The command to run tests can be customized as follows:

1. Create a file: run/run-test.sh.
2. Add the following content to this file and customise the docker compose command as needed:
[source,sh]
----
#!/usr/bin/bash

set -ex
#Override this command to do something fance for your project
#If this file doesn't exist, the defaul command, shown below, is executed.
docker-compose -f 'docker-compose.yml' up
----

The test phase can typically run multiple containers. The container running the actual tests should write the '/test/done'
file to indicate tests are finished. Each container, started in test mode, will monitor this file and gracefully shutdown
after it's creation. In order to do so the following file, available in /usr/bin/check_test.sh via the base images, can
be forked in the background from the main entrypoint:

[source,sh]
----
#!/bin/bash

while  [ ! -f  "/test/done" ]; do echo "Waiting for tests to finish"; sleep 1; done

echo "Tests are done, stopping container"

PID=$(pgrep -f $1)
kill -s TERM "${PID}"
----

== To use

[source,sh]
----
build.sh [-lt]

  -b, --build      Build docker image
  -r, --release    Push docker image to registry
  -t, --test       Execute tests

  -l, --local      Run workflow locally in a local docker container
  -v, --verbose    Run in verbose mode
  -f, --force      Force running the build in a fresh environment, requires
                   internet access to pull dependencies. Otherwise internet
                   access is only needed for the first pull of the precompiled
                   build environment image
  -n, --no-export  Don't export the build artiface, this is used when running
                   the build workflow locally

  -h, --help       Show help
----

=== Managing external data

During image building external data (e.g. releases) is often needed. In order to accomodate fetching external data the
copy_data.sh script has been provided. Two methods are defined in this script:

[source,sh]
----
#!/bin/bash

init_data (){
    LOCAL=0
    if [ "$1" == "local" ]; then
        LOCAL=1
    fi

    if [ "${LOCAL}" -eq 0 ]; then
        #Remote / gitlab ci
        echo -n ""
    else
        #Local copy
        echo -n ""
    fi
}

cleanup_data () {
    echo -n ""
}
----

As you can see `init_data` supports two scenarios. one for local copy actions and one for gitlab ci integrated copy
actions. This distinction is typically used to download releases (e.g. from b2drop) during gitlab ci workflows and to
copy in local files during local build / development cycles.

`cleanup_data` should implement cleanup commands to remove all files created / downloaded during the `init_data` phase.

An example can be found here: https://gitlab.com/CLARIN-ERIC/docker-aai-discovery/blob/master/copy_data.sh.

=== Locally

==== Building

[source,sh]
----
sh build.sh --build --local
----

==== Testing

[source,sh]
----
sh build.sh --test --local
----

==== Releasing

[source,sh]
----
sh build.sh --release --local
----

=== Integrated in GitLab CI

Add a .gitlab-ci.yml file to your repository with the following content:
[source,sh]
----
image: docker:1.12.1
services:
  - docker:1.12.1-dind

stages:
  - build
  - test
  - release

build:
  artifacts:
    untracked: true
  script: timeout -t 180 sh -x ./build.sh --build
  stage: build
  tags:
    - docker

test:
  artifacts:
    untracked: true
  dependencies:
    - build
  script: timeout -t 180 sh -x ./build.sh --test
  stage: test
  tags:
    - docker

release:
  artifacts:
    untracked: true
  dependencies:
    - test
  only:
    - tags
    - triggers
  script: timeout -t 120 sh -x ./build.sh --release
  stage: release
  tags:
    - docker
----


